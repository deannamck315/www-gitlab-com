---
layout: handbook-page-toc
title: "Leading Organizations"
description: "Leading Organizations are groups and people who consistently make meaningful contributions to GitLab."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

GitLab's [mission](/company/mission/#mission) is to make it so everyone can contribute. 

The GitLab Leading Organization program supports this mission by recognizing and incentivizing organizations who are among our most active contributors. This initiative rewards and encourages co-creation. Through this program, we seek to strengthen the open source ecosystem. Leading Organizations drive their company’s innovation and employee retention through contributions while influencing GitLab’s product trajectory. 

Our goal is for 10% of GitLab customers to be Leading Organizations by FY25Q4. This supports our [strategy](/company/strategy/#2-build-on-our-open-core-strength) to grow to 1000 contributors per month.

## Qualification

See the definition for Leading Organizations in the [code review section](/handbook/engineering/workflow/code-review/#leading-organizations)

Eligibility for existing Leading Organizations will be reviewed [quarterly](/handbook/communication/#communicating-dates-and-time).

## Why

Open source is eating the world. The organizations that will stand out as leaders in the next decade are those who understand, embrace and contribute to open source - building cultures where everyone can contribute.

### Increase Hiring and Retention

Lack of developer talent is one of the biggest threats to businesses. Contributing to open source enables your company to be competitive in a tight market

1. Highly skilled developers often participate heavily in open source projects and seek out companies with an active open source presence 
1. Public recognition builds employer brand and incentivizes individual employees
1. Employment satisfaction increases through skill building, training and knowledge sharing

### Reduce Cost & Improve Innovation

80% of companies expect to increase their use of enterprise open source software for emerging technologies. [^1]

1. Paying employees to contribute to Open Source can double the company’s productivity, when compared to non-contributing competitors [^2]
1. Open source saves time and money while increasing efficiency 
1. Consistent contributing enables organizations to replace point solutions faster while reducing technical debt

## Program Benefits

Leading Organizations receive: 

- Faster time to review [service level objective](/handbook/engineering/workflow/code-review/#review-response-slo) from GitLab team members
- Certainty that if you need something added to GitLab, you have a path to ensuring that you get it
- Public recognition with special Leading Organization badge visible on LinkedIn and other social channels
- Increased employer brand visibility through blog posts on GitLab.com, contributor stories and social media
- Access to GitLab through Engineers, Product Managers, Community Relations, and other Core Members to provide guidance and answer questions on MRs
- General guidance on the legal considerations of contributing to open source and open core software projects

## Enrollment

Process for applying to the Leading Organization program: 

1. Fill out [form](https://forms.gle/HRWyXBKkgbnwhUdU6) to receive more information
1. Fill out issue to apply [link to come] 
1. Welcome Email, once accepted
1. Schedule an onboarding workshop

## Workshops

GitLab hosts workshops with organizations interested in increasing their contributions and becoming eligible for the program benefits. Leading Organization team will reach out directly to organizations that either meet or are close to the threshold. For organizations interested in starting to contribute, please fill out [this form](https://forms.gle/HRWyXBKkgbnwhUdU6).

**Types of Workshops**

1. Program Workshop - Program Overview and Benefits, Audience: Business Stakeholders, GitLab: Leading Organization Team and CSM/Account Executive (optional
2. Onboarding Workshop - Technical Deep Dive -  How to start/increase contributions, Audience: Technical Leadership, Engineers interested in contributing. Meet with Contributor Success Team on open issues and scope out new requests. 

## Tips from GitLab on how to accelerate your organizations contribution rate

- Ensure your contributions are being counted by entering your Organization’s name into your GitLab profile details
- Enable team-members to make their [first contributions](/community/hackathon/)
- Join [Monthly Hackathons](/community/hackathon/)
- Allocate dedicated time to team members to contribute back to open source.

## Communication with GitLab

Email: [leadingorganizations AT gitlab DOT com](mailto:leadingorganizations@gitlab.com)

Contribution Help: 
- [Community Relations team](/handbook/marketing/community-relations/)
- [Contributor Success team](/handbook/engineering/quality/contributor-success/)
- [Core team](/community/core-team/)
- [MR Coaches](/job-families/expert/merge-request-coach/)

## Who

The Leading Organizations program is a cross-functional initiative that is led by [Nick Veenhof](/company/team/#nick_vh), [Contributor Success](/handbook/engineering/quality/contributor-success/) team and [John Coghlan](/company/team/#johncoghlan), [Community Relations team](/handbook/marketing/community-relations/). 

This program is part of the [Contribution Efficiency Initiative](https://about.gitlab.com/handbook/marketing/community-relations/code-contributor-program/collaborations/) and a current top [cross-functional initiative](https://about.gitlab.com/company/top-cross-functional-initiatives/#current-top-cross-functional-initiatives), [User Engagement](https://gitlab.com/groups/gitlab-com/-/epics/1794).

- Executive Sponsor: [Ashley Kramer](/company/team/#akramer), Chief Marketing and Strategy Officer, Interim Chief Technology Officer
- DRI: [Liz Petoskey](/company/team/#epetoskey), Director of Strategy and Operations


[^1]: RedHat, The State of Enterprise Open Source (February 22, 2022). Available at: [https://www.redhat.com/en/resources/state-of-enterprise-open-source-report-2022](https://www.redhat.com/en/resources/state-of-enterprise-open-source-report-2022)

[^2]: Nagle, Frank, Learning by Contributing: Gaining Competitive Advantage Through Contribution to Crowdsourced Public Goods (December 21, 2017). Available at SSRN: [https://ssrn.com/abstract=3091831](https://ssrn.com/abstract=3091831)
